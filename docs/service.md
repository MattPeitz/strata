# Strata.js Service Instance

Strata.js is all about building a "service". However, it's useful to have a single object that can be used both
internally and externally for interacting with the various pieces of what Strata.js does. That single object is
the `service` instance.

The `service` instance allows you to do things like register contexts, global middleware, load configuration, and most
importantly, it allows you to start listening for incoming connections. The service context is always the same instance
per process, and is safe to store (but you can always access it via an import).

## Accessing the Service Instance

Accessing the service instance is as simple as an import statement:

```typescript
import { service } from '@strata-js/strata';

// That's all!
```

## Using the Service Instance

The `service` instance has a pretty simple, easy to use API. It provides a very thin surface of things the developer can
do to build and control a running service.

### Creating a Service

Creating a service is as simple as importing `service` from Strata.js, and then calling `service.init()`. However, when
you start a service you are required to pass it a service name, and (if you haven't already) a configuration. Even
though the configuration is optional, it should noted that failing to provide a configuration will cause Strata.js to
attempt to connect to local host for it's redis connection, which is likely not what you want.

```typescript
import { service } from '@strata-js/strata';

import config from './config';

// ...do things, like register contexts...

service.init('MyService', config)
    .then(() =>
    {
        // Do any post-startup code.
    });
```

### Registering Middleware

While this has been covered in several places, registering global middleware is easy with the `service` instance. Simply
call `service.useMiddleware()` and pass it the middleware you want to register.

(See [Strata.js Middleware](middleware.md) for more information.)

### Registering Contexts

This has been covered by our Context documentation. But to recap, simply call `service.registerContext()` with the name
of the context, and the context instance.

(See [Strata.js Contexts](contexts.md) for more information.)

### Loading Service Config

Strata.js provides several ways to load configuration. The main reason for this is that we don't want to control how a
service may want to get its configuration. It may want to use a database for the configuration, or load it from a rest
call, or any one of an infinite number of methods. Instead, we provide the tools to work with config once you've loaded
it.

The primary way to load configuration is with `service.parseConfig()` This will parse configuration based on the passed
in environment, and supports environmental overrides.

However, if you wish to just set the configuration and skip all of that fancy stuff, you may do so
with `service.setConfig()`. It directly sets the configuration.

Both of these may be called prior to calling `service.init()`, at which point passing a configuration is optional.

(See [Strata.js Configuration](configuration.md) for more information.)

### Starting the Service

As was covered in [Creating a Service](#creating-a-service), the service is started with a call to `service.init()`,
which takes the name of the service, and (optionally) a configuration. This returns a promise, which once fulfilled,
indicates that the service is listening.

### Making Service to Service calls

If your service needs to call other servics, it can do so with `service.request()`:

```typescript
import { service } from '@strata-js/strata';

// This puts a request on the `MyServiceBus` queue, with a context of `myContext` and an operation of `myOperation`.
service.request('MyServiceBus', 'myContext', 'myOperation', {
    doStuff: true,
    foo: "bar"
}, { meta: "data" });
```

This is the prefered way to make requests from inside the service.
