# Strata.js Documentation

Currently, our documentation covers most, but not all topics, and it loosely organized. At some point in the future,
this will be turned into a seperate Guide and API documentation, similiar to [VueJS](https://vuejs.org/).

_This is a rough order to the current documentation that should build on itself, but the current documentation is
heavily technically detailed, without much of an attempt to introduce concepts. This will be improves in the future._

* [How Strata.js Works](./overview.md)
* [Redis Streams Usage](./redis-streams.md)
* [On-wire Message Protocol](./protocol.md)
* [Strata.js Contexts](./contexts.md)
* [Strata.js Middleware](./middleware.md)
* [Basics of Strata.js Services](./service.md)
* [Basics of Strata.js Clients](./client.md)
* [How to configure a service](./configuration.md)
