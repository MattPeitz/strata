# Strata.js Request Object

This object represents the full request throughout the request lifecycle. It contains the initial request properties, as
well as response properties, as the response is built.

_Note: More documentation coming._
