# How Strata.js uses Redis Streams

Strata.js is built on top of `io-redis`, using the [redis streams][streams] API. It allows for a very robust message
queue setup, and very performant services. Redis streams allows us to have all the features of larger message queue
services, like [kafka][] or [rabbitmq][], without the overhead (both in terms of performance, and server deployment.)

What follows is an overview of how we use the Redis Streams API to build a reliable queue with observability, built-in
retry of messages, and scalability.

## Service Usage

Strata.js's main usage is writing services that use Redis Streams. These services have to listen for incoming messages,
and respond appropriately.

### Setup

We must do the following as setup:

1. Create a unique ID for the service (hostname, or passed in)
2. Create the consumer group and stream
    * `XGROUP CREATE queueName serviceName $ MKSTREAM`
        * If it fails with `BUSYGROUP`, we continue on; that means the group already existed.
3. Make two connections to redis; one for listening for requests, the other for sending responses.

### Listening for requests

To listen for requests, we do the following command:

`XREADGROUP GROUP serviceName serviceID COUNT serviceConcurrency BLOCK 0 STREAMS queueName >`

This will return once there's data to read. We will get up to `serviceConcurrency` messages to process. We parse all the
messages, and add them to our outstanding requests. We then start processing them, hitting middleware, operation
handlers, etc. Once the message is either succeeded or failed, we move on to sending the response.

_Note: It is important we record off the message ID from redis, so we can `XACK` them when responding._

#### Queue Names

By default, we use the convention `Requests:${ serviceName }` for streams. We recommend, in environments where 
developers need to run their own copies of services on the same bus as a global group of services, that they append 
`${ hostname() }` on the end of the queue name.

### Sending a response

Once a response has been succeeded or failed, and run through all the middleware, it is rendered out to the response
envelope format. We then start a _pipeline_ command and send the following:

`XADD queueName * msg "{...}"`
`XACK queueName serviceName requestID`

The reason for using a pipeline is that we need to both add the response, and `XACK` the request in the same message to
redis as a single transaction. This will allow us to make sure either we don't send the message, or we do, and we
acknowledge it, even in the event of a catastrophic termination of the service.

---

## Client Usage

Strata.js can also be used as a client, either directly, or for service to service calls.

### Setup

We must do the following as setup:

1. Create a unique responseQueue string that we will be listening for responses on. This queue is the same for the
   lifetime of the client process.
    * This should be of the form: `Responses:${ serviceName }:${ serviceID }`,
      i.e. `Responses:MyExampleService:MyComputer`.
2. Make two connections to redis; one for listening for responses, the other for sending requests.
3. Make a stream of the name of our responseQueue.

### Making a request

_Question: Should we use service discovery to check to see if a service of that name is alive, and fail the request if
it is not? One of the things we should do is fail if there's no stream of that name, and have a process that murders
streams for services that aren't connected anymore. That might be sufficient. But we could always go make the check
ourselves._

To make a request, we do the following command:

`XADD serviceQueueName * msg "{ ... }"`

The `serviceQueueName` is the name of the service queue we want to send the message to.

### Listening for responses

To listen we simply do the following command:

`XREAD BLOCK 0 STREAMS Responses:MyExampleService:MyComputer`

This will return once there's data to read. We parse all the messages, and pair them with outstanding requests. Any
messages that we receive that are not for us, we log a warning about and skip. We then correctly fail or succeed the
message and allow the code to handle that as it will.

[streams]: https://redis.io/topics/streams-intro

[kafka]: https://kafka.apache.org/

[rabbitmq]: https://www.rabbitmq.com/
