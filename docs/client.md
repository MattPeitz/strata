# Strata.js Client Instance

The Strata.js client instance is much like the [service instance](./service.md), except that while a client instance is
still provided for you, you can instantiate your own client instances, or even inherit from the normal client to
customize behavior.

## Accessing the Build-in Client Instance

In typical usage, service code does not need to use the client instance directly. It's a private property of
the `service` instance, and as such isn't exposed. All of the client functionality that's relevant to user code is
exposed via the `service` instance.

## Creating a Client

If, instead, you need to build software that communicates to Strata.js services, you will need to create your own Client
instance. We have made that very easy to do:

```typescript
import { Client } from

`@strata-js/strata`

// Create the client
const client = new Client();

// Once you're ready for the client to make response queues, you will call `init`.
client.init('MyClientName', { host: 'localhost' })
    .then(() =>
    {
        // Do any post-startup code.
    });

// Now you can use the client
```

### Inheriting from Client

If you need to make modifications to the client logic, you can easily inherit from the client, and override behavior.
But, be ware, there be dragons here. Make sure you understand the functionality you're modifying, as you could introduce
subtle bugs, like improper shudtdown logic, or message loss.

## Using the Client

The `client` instance has a pretty simple, easy to use API. It provides a very thin surface of things the developer can
do to control a running client.

### Making Requests

If your service needs to call other servics, it can do so with `client.request()`:

```typescript
import { Client } from '@strata-js/strata';

// Create the client
const client = new Client();

// Assume client is initiated at some point

// This puts a request on the `MyServiceBus` queue, with a context of `myContext` and an operation of `myOperation`.
await const response = client.request('MyServiceBus', 'myContext', 'myOperation', {
    doStuff: true,
    foo: "bar"
}, { meta: "data" });
```

This is exactly like `service.request()`, which is, in fact, a pass through to `client.request()`.
