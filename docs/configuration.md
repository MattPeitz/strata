# Configuration Manager

Strata.js has a build in configuration manager with support for many scenarios of configuration. It was designed with
the idea of being able to be used for development (or with file-based configuration) very easily, while also supporting
more advanced configuration management at the same time.

## Loading configuration

There are two methods for loading a configuration into the configuration manager: `setConfig` and `parseConfig`. While
the names may be similar, they do two very different things. Both are exposed to cover all possible use cases.

### `setConfig(config : Record<string, unknown>) : void`

The `setConfig` function is as simple as it gets. It takes a configuration, and it sets it. There is nothing special
about the configuration, except that it conform to the type `Record<string, unknown>`.

### `parseConfig(config : Record<string, unknown>) : void`

The `parseConfig` function is where the magic happens. This function supports several very handy short cuts, which
require we parse the configuration file in order to build the correct configuration. Once that config is built, we
call `setConfig` ourselves.

#### Environment-based configurations

The first thing `parseConfig` looks for, is a configuration object that has an `environments` key. (This is not
required, but it is one of the two major reasons to use `parseConfig`.) What it then does is to look for
the `ENVIRONMENT` or `NODE_ENVIRONMENT` variable, and then apply any configuration found
under `environments[ENVIRONMENT]`.

Let's look at an example. Given the following configuration:

```typescript
// config.ts
export default {
    http: {
        secure: false,
        port: 12345
    },

    environments: {
        test: {
            http: {
                secure: true
            }
        },
        production: {
            http: {
                secure: true,
                port: 443
            }
        }
    }
}
```

Then the following will be true:

```typescript
import config from './config.ts';
import { configUtil } from '@strata-js/strata';

//---------------------------------------

// Set our environment to 'local'
process.env.ENVIRONMENT = 'local';
configUtil.parseConfig(config);

// { secure: false, port: 12345 }
console.log(configUtil.get('http'));

//---------------------------------------

// Set our environment to 'test'
process.env.ENVIRONMENT = 'test';
configUtil.parseConfig(config);

// { secure: true, port: 12345 }
console.log(configUtil.get('http'));

//---------------------------------------

// Set our environment to 'production'
process.env.ENVIRONMENT = 'production';
configUtil.parseConfig(config);

// { secure: true, port: 443 }
console.log(configUtil.get('http'));
```

This allows one configuration file to contain the details about multiple environments, while reusing most of the default
configuration. If an environment isn't specified, the default configuration is used.

#### Environment Variable config overriding

The other feature of `parseConfig` is support for overriding sections of config with environment variables. It looks for
any environment variables that start with `CONFIG.`, and then replaces the values of the config at that location with
the value of the environment variable. In order to handle conversions from the traditional UPPERCASE of environment
variables, we are passing this to lodash's [camelCase][] function. This should be sufficient for most use cases.

**WARNING: Due to the nature of environment variables, the type for any overridden property is `string`.**

Let's look at an example. Give the configuration:

```typescript
// config.ts
export default {
    http: {
        secure: false,
        port: 12345
    },

    environments: {
        test: {
            http: {
                secure: true
            }
        },
        production: {
            http: {
                secure: true,
                port: 443
            }
        }
    }
}
```

Then the following will be true:

```typescript
import config from './config.ts';
import { configUtil } from '@strata-js/strata';

//---------------------------------------

// Override our default config
process.env['CONFIG.HTTP.PORT'] = '4545';
configUtil.parseConfig(config);

// { secure: false, port: 4545 }
console.log(configUtil.get('http'));

//---------------------------------------

// We support mixing both types of configuration
process.env.ENVIRONMENT = 'test';
process.env['CONFIG.HTTP.PORT'] = '4545';
configUtil.parseConfig(config);

// { secure: true, port: 4545 }
console.log(configUtil.get('http'));

//---------------------------------------

// The environment variables take precedence over the environment configurations.
process.env.ENVIRONMENT = 'production';
process.env['CONFIG.HTTP.PORT'] = '4545';
configUtil.parseConfig(config);

// { secure: true, port: 4545 }
console.log(configUtil.get('http'));
```

### Loading from an external source

Because of how the configuration manager is written, it's trivial to load the configuration from an external source.
Here are a few examples:

```typescript
import $http from 'axios';
import { configUtil } from '@strata-js/strata';

//---------------------------------------

// Local config file
configUtil.parseConfig(await import('./config.ts'));

// HTTP call
configUtil.setConfig(JSON.parse(await $http.get('https://example.com/config/my-service/production.json')));

```

[camelCase]: https://lodash.com/docs/#camelCase
