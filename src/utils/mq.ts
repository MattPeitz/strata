//----------------------------------------------------------------------------------------------------------------------
// MessageQueueUtil
//----------------------------------------------------------------------------------------------------------------------

import { Redis } from 'ioredis';
import { logging } from '@strata-js/util-logging';

// Interfaces
import { ResponseEnvelope } from '../interfaces/response';
import { RequestEnvelope } from '../interfaces/request';
import { DiscoveredQueues } from '../interfaces/discovery';

// Manager
import serviceMan from '../managers/service';

// Resource Access
import redisRA from '../resource-access/redis';

// Classes
import { StrataRequest } from '../lib/request';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('mqUtil');

type Handler<T> = (message : T) => Promise<void>;
type WaterMarkCheck = () => Promise<boolean>;

//----------------------------------------------------------------------------------------------------------------------

export class MessageQueueUtil
{
    // Redis connections
    readonly #controlConn : Redis;
    readonly #listenConn : Redis;
    readonly #sendConn : Redis;

    // Store the listen connection clientID, for unblocking
    #listenClientID = 'unknown';

    // Are we currently listening for new messages?
    #listening = false;

    //------------------------------------------------------------------------------------------------------------------

    constructor(connectionConfig : Record<string, unknown>)
    {
        this.#controlConn = redisRA.getConnection(connectionConfig);
        this.#listenConn = redisRA.getConnection(connectionConfig);
        this.#sendConn = redisRA.getConnection(connectionConfig);
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Internal helpers
    //------------------------------------------------------------------------------------------------------------------

    private async _listenLoop<Envelope extends RequestEnvelope | ResponseEnvelope>(
        queue : string,
        name : string,
        id : string,
        handler : Handler<Envelope>,
        waterMarkCheck : WaterMarkCheck = async() => true
    ) : Promise<void>
    {
        if(await waterMarkCheck())
        {
            const message = await redisRA.blockingRead(this.#listenConn, queue, name, id);
            if(message)
            {
                // TODO: There's no enforcement here! We should have some validation the envelope is correctly formatted!
                // This should be AVJ based.
                const envelope : Envelope = {
                    ...message.msg as Envelope,
                    internalID: message.id
                };

                // Call the handler, but don't wait on it. Regardless of what happens, remove the message from our list.
                handler(envelope);
            }
        }
        else
        {
            // If we're at our high-water mark, let's wait for 50ms, and then try taking messages.
            await new Promise((resolve) => setTimeout(resolve, 50));
        }

        // If we're still listening, we call ourselves recursively. We do _not_ await the promise.
        if(this.#listening)
        {
            this._listenLoop<Envelope>(queue, name, id, handler, waterMarkCheck);
        } // end if
    } // end _listenLoop

    //------------------------------------------------------------------------------------------------------------------
    // Public API
    //------------------------------------------------------------------------------------------------------------------

    public async init(name : string, queue : string, id : string) : Promise<void>
    {
        // Get the listen connection's client id
        this.#listenClientID = await redisRA.getClientID(this.#listenConn);

        // Ensure the consumer group
        await redisRA.ensureConsumerGroup(this.#listenConn, queue, name);

        // Create our consumer
        await redisRA.createConsumer(this.#listenConn, queue, name, id);
    }

    public listen<Envelope extends RequestEnvelope | ResponseEnvelope>(
        queue : string,
        name : string,
        id : string,
        handler : Handler<Envelope>,
        waterMarkCheck : WaterMarkCheck = async() => true
    ) : void
    {
        if(this.#listenConn)
        {
            this.#listening = true;

            // Start the listening loop
            this._listenLoop<Envelope>(queue, name, id, handler, waterMarkCheck);
        }
        else
        {
            logger.error(`Attempted to listen, but there is no connection.`);
        } // end if
    } // end listen

    public async stopListeningForResponse(queue : string, name : string, id : string) : Promise<void>
    {
        // Stop listening
        this.#listening = false;

        // Unblock the listen connection
        await redisRA.unblock(this.#controlConn, this.#listenClientID);

        // Delete Consumer
        await redisRA.deleteConsumer(this.#controlConn, queue, name, id);
    } // end stopListeningForResponse

    public async discoverQueues() : Promise<DiscoveredQueues>
    {
        const results = {};
        const queues = await redisRA.listQueues(this.#controlConn);

        await Promise.all(queues.map(async(queue) =>
        {
            const groups = await redisRA.listGroups(this.#controlConn, queue);

            await Promise.all(groups.map(async(group) =>
            {
                group.consumers = await redisRA.listConsumers(this.#controlConn, queue, group.name as string);
            }));

            results[queue] = {
                groups
            };
        }));

        return results;
    }

    public async request(queue : string, request : StrataRequest) : Promise<string | undefined>
    {
        if(this.#sendConn)
        {
            // We have to do this downcast, because while it could, technically, lead to unsafe code, we know all we're
            // doing is passing the object along, not manipulating it.
            const requestEnv = request.renderRequest() as unknown as Record<string, unknown>;
            return redisRA.insertMessage(this.#sendConn, queue, requestEnv);
        }
        else
        {
            logger.error(`Attempted to make a request, but there is no connection.`);
        } // end if
    } // end request

    public async respond(request : StrataRequest) : Promise<void>
    {
        if(this.#sendConn)
        {
            if(request.status === 'pending')
            {
                request.fail('Service error, attempted to respond while request still pending.');
                logger.warn('Attempting to respond with a pending status! Failing request.');
            } // end if

            if(!request.responseQueue)
            {
                logger.debug(`Ignoring attempt to respond to request '${ request.id }'; no response queue.`);
            }
            else
            {
                // We have to do this downcast, because while it could, technically, lead to unsafe code, we know all we're
                // doing is passing the object along, not manipulating it.
                const responseEnv = request.renderResponse() as unknown as Record<string, unknown>;

                // Send the response
                await redisRA.insertMessage(this.#sendConn, request.responseQueue, responseEnv);

                // Ack the original message
                if(request.internalID)
                {
                    await redisRA.ackMessage(this.#controlConn, serviceMan.service.queue, serviceMan.service.name, request.internalID);
                } // end if
            } // end if
        }
        else
        {
            logger.error(`Attempted to send response, but there is no connection.`);
        } // end if
    } // end respond

    async teardown() : Promise<void>
    {
        // Disconnect from redis
        this.#controlConn.disconnect();
        this.#listenConn.disconnect();
        this.#sendConn.disconnect();
    } // end teardown
} // end MessageQueueUtil

//----------------------------------------------------------------------------------------------------------------------
