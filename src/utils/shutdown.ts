// ---------------------------------------------------------------------------------------------------------------------
// Signal Handling Util
// ---------------------------------------------------------------------------------------------------------------------

import { StrataClient } from '../lib/client';
import envConfig from '@strata-js/util-env-config';
import { logging } from '@strata-js/util-logging';

// Managers
import serviceMan from '../managers/service';

// ---------------------------------------------------------------------------------------------------------------------

type Signal = 'SIGTERM' | 'SIGINT' | 'SIGBREAK' | string;

const logger = logging.getLogger('shutdownUtil');

// ---------------------------------------------------------------------------------------------------------------------

class ShutdownUtil
{
    #isShuttingDown = false;
    #interruptsToForceShutdown : number;
    #shutdownTimeout : number;
    #clients : Set<StrataClient> = new Set();

    constructor()
    {
        this.#interruptsToForceShutdown = envConfig.get('interruptsToForceShutdown', 3);
        this.#shutdownTimeout = envConfig.get('interruptsToForceShutdown', 30 * 1000);

        this.$handleKeyInterrupt('SIGBREAK');
        this.$handleKeyInterrupt('SIGINT');

        // Also listen for signals to start shutdown, but not force it.
        this.$gracefulShutdownOnSignal('SIGTERM');
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Properties
    // -----------------------------------------------------------------------------------------------------------------

    get isShuttingDown() : boolean { return this.#isShuttingDown; }

    // -----------------------------------------------------------------------------------------------------------------
    // Helpers
    // -----------------------------------------------------------------------------------------------------------------

    private $gracefulShutdownOnSignal(signal : Signal) : void
    {
        process.on(signal, () =>
        {
            logger.info(`Got ${ signal }; shutting down gracefully...`);
            this.gracefulShutdown();
        });
    }

    private $handleKeyInterrupt(signal : Signal) : void
    {
        process.on(signal, () =>
        {
            this.#interruptsToForceShutdown--;

            if(this.#isShuttingDown)
            {
                if(this.#interruptsToForceShutdown > 0)
                {
                    logger.warn(`Key Interrupt (${ signal }): Already shutting down; interrupt ${ this.#interruptsToForceShutdown } more time(s) to force shutdown.`);
                }
                else
                {
                    logger.warn(`Key Interrupt (${ signal }): Forcing shutdown.`);
                    process.exit(1);
                }
            }
            else
            {
                logger.warn(`Key Interrupt (${ signal }): Shutting down; interrupt ${ this.#interruptsToForceShutdown } more time(s) to force shutdown.`);
                this.gracefulShutdown();
            }
        });
    }

    private async $timeout(fn : () => Promise<any>, timeout : number) : Promise<void>
    {
        if(timeout > 0 && Number.isFinite(timeout))
        {
            return new Promise((resolve, reject) =>
            {
                const timeoutHandle = setTimeout(() =>
                {
                    reject(new Error('Timeout'));
                }, timeout);

                fn()
                    .then(resolve)
                    .catch(reject)
                    .finally(() =>
                    {
                        clearTimeout(timeoutHandle);
                    });
            });
        }
        else
        {
            return fn();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Public
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Initiates a graceful shutdown (i.e. it waits for incoming messages to be processed, and outgoing messages to be
     * completed as well.)
     *
     * Under normal circumstances, this shouldn't be called by a developer's code, instead, it should be called by
     * internal signal handlers.
     */
    public async gracefulShutdown() : Promise<void>
    {
        if(!this.#isShuttingDown)
        {
            this.#isShuttingDown = true;

            logger.info('Shutdown started.');

            // Shutdown service
            if(serviceMan.service.initialized)
            {
                // -----------------------------------------------------------------------------------------------------
                // Shutdown Listening
                // -----------------------------------------------------------------------------------------------------

                logger.info(`Shutting down incoming request listening with ${ serviceMan.service.outstandingRequests.size } messages outstanding.`);

                // We wait for messages to process, but with a (possible) timeout.
                await this.$timeout(() => serviceMan.service.teardown(), this.#shutdownTimeout)
                    .then(() =>
                    {
                        logger.info('Successfully shut down incoming request listening.');
                    })
                    .catch((error) =>
                    {
                        if(error.message.includes('Timeout'))
                        {
                            logger.warn(`Timeout expired with ${ serviceMan.service.outstandingRequests.size } messages outstanding.`);
                        }
                        else
                        {
                            logger.error('Error encountered while shutting down incoming requests:', error.stack);
                        }
                    });
            }

            // ---------------------------------------------------------------------------------------------------------
            // Shutdown Requests
            // ---------------------------------------------------------------------------------------------------------

            logger.info(`Shutting down ${ this.#clients.size } clients with outstanding requests.`);

            // We shut down all clients at the time time, but give each of them their own timeout.
            await Promise.all([ ...this.#clients ].map(async(client) =>
            {
                if(client.initialized)
                {
                    logger.info(`Shutting down client ${ client.name }(${ client.id }) with ${ client.outstandingRequests.size } outstanding requests.`);
                    await this.$timeout(() => client.teardown(), this.#shutdownTimeout)
                        .then(() =>
                        {
                            logger.info(`Successfully shut down client ${ client.name }(${ client.id }).`);
                        })
                        .catch((error) =>
                        {
                            if(error.message.includes('Timeout'))
                            {
                                logger.warn(`Timeout expired for client ${ client.name }(${ client.id }) with ${ client.outstandingRequests.size } messages outstanding.`);
                            }
                            else
                            {
                                logger.error('Error encountered while shutting down incoming requests:', error.stack);
                            }
                        });
                }
            }));

            // ---------------------------------------------------------------------------------------------------------
            // Complete Shutdown
            // ---------------------------------------------------------------------------------------------------------

            logger.info('Graceful shutdown complete. Exiting.');

            process.exit(0);
        }
        else
        {
            logger.warn(`Already shutting down; gracefulShutdown() called multiple times. Ignoring.`);
        }
    }

    /**
     * Registers clients with the shutdown handler, so we can wait for them to finish cleaning up when performing a
     * graceful shutdown.
     *
     * @param client - The client to register.
     */
    registerClient(client : StrataClient) : void
    {
        this.#clients.add(client);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new ShutdownUtil();

// ---------------------------------------------------------------------------------------------------------------------
