//----------------------------------------------------------------------------------------------------------------------
// OperationLocalStorage
//----------------------------------------------------------------------------------------------------------------------

import { AsyncLocalStorage } from 'async_hooks';

// Interfaces
import { LocalStorage } from '../interfaces/localStorage';

//----------------------------------------------------------------------------------------------------------------------

type RunFunction = () => Promise<unknown>

//----------------------------------------------------------------------------------------------------------------------

class OperationLocalStorageUtil
{
    #asyncLocalStorage = new AsyncLocalStorage();

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Runs a function inside of an AsyncLocalStorage context. The `store` parameter is set as the store for this run
     * context.
     *
     * _Note: A "run context" is simply the chain of function calls (synchronous or asynchronous) triggered in the
     * execution of `func`. I.e. everything called inside of `func` will be inside the "run context"._
     *
     * @param store - The store for this run context.
     * @param func - The function to run in the context.
     */
    public async run(store : LocalStorage, func : RunFunction) : Promise<void>
    {
        await this.#asyncLocalStorage.run(store, () =>
        {
            return func();
        });
    } // end run

    /**
     * Retrieves the current store for this run context.
     *
     * @returns Returns either the store, or undefined if no store has been set, or we're outside a run context.
     */
    public getStore() : LocalStorage | undefined
    {
        return this.#asyncLocalStorage.getStore() as (LocalStorage | undefined);
    } // end getStore
} // end OperationLocalStorage

//----------------------------------------------------------------------------------------------------------------------

export default new OperationLocalStorageUtil();

//----------------------------------------------------------------------------------------------------------------------
