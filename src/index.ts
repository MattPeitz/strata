// ---------------------------------------------------------------------------------------------------------------------
// Strata Main Module
// ---------------------------------------------------------------------------------------------------------------------

import envConfig from '@strata-js/util-env-config';

// Managers
import serviceMan from './managers/service';

// Utils
import localStorage from './utils/localStorage';

// ---------------------------------------------------------------------------------------------------------------------

// Export Internal classes
export { StrataContext as Context } from './lib/context';
export { StrataRequest as Request } from './lib/request';
export { StrataClient as Client } from './lib/client';
export * as errors from './lib/errors';

// Export Interfaces
export { RequestEnvelope } from './interfaces/request';
export { ResponseEnvelope } from './interfaces/response';

// Export Managers
export * from '@strata-js/util-logging';

// Export Utils
export { envConfig as configUtil, localStorage };

// Export the service instance
export const { service } = serviceMan;

// ---------------------------------------------------------------------------------------------------------------------
