//----------------------------------------------------------------------------------------------------------------------
// ContextManager
//----------------------------------------------------------------------------------------------------------------------

import { StrataContext } from '../lib/context';
import { UnknownContextError } from '../lib/errors';

//----------------------------------------------------------------------------------------------------------------------

class ContextManager
{
    readonly #contexts : Map<string, StrataContext> = new Map();

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Registers a context with the service. All registered operation functions will be exposed via this context.
     *
     * @param contextName - The name to expose this context under.
     * @param context - The context to register.
     */
    public register(contextName : string, context : StrataContext) : void
    {
        this.#contexts.set(contextName, context);
    } // end register

    /**
     * Retrieves a context by name. If the context isn't found, it throws an error.
     *
     * @param contextName - The context name to retrieve.
     *
     * @throws Throws an error in the event a context isn't found. The caller is expected to handle this and fail any
     * requests that triggered retrieving this context.
     *
     * @returns Returns the requested context.
     */
    public getContext(contextName : string) : StrataContext
    {
        const context = this.#contexts.get(contextName);
        if(!context)
        {
            throw new UnknownContextError(contextName);
        } // end if

        return context;
    } // end getContext

    /**
     * Gets a list of valid context names.
     *
     * @returns Returns a list of all the valid contexts, by name.
     */
    public getContextNames() : string[]
    {
        return Array.from(this.#contexts.keys());
    } // end getContextNames

    /**
     * Clears all registered contexts.
     *
     * _Note: This is primarily for unit testing. _
     */
    public clearContexts() : void
    {
        this.#contexts.clear();
    }
} // end ContextManager

//----------------------------------------------------------------------------------------------------------------------

export default new ContextManager();

//----------------------------------------------------------------------------------------------------------------------
