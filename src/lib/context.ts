//----------------------------------------------------------------------------------------------------------------------
// StrataContext
//----------------------------------------------------------------------------------------------------------------------

// Interfaces
import { OperationHandler } from '../interfaces/context';
import { OperationMiddleware } from '../interfaces/middleware';

// Message
import { StrataRequest } from './request';

// Errors
import { UnknownOperationError } from './errors';
import { ValidMetadataType, ValidPayloadType } from '../interfaces/payload';

//----------------------------------------------------------------------------------------------------------------------

type OperationConfig = {
    opFunc : OperationHandler<any, any>,
    middleware : OperationMiddleware[]
};

type OperationMiddlewareIterable = Set<OperationMiddleware> | OperationMiddleware[];

//----------------------------------------------------------------------------------------------------------------------

export class StrataContext
{
    readonly #operations : Map<string, OperationConfig> = new Map();
    readonly #middleware : Set<OperationMiddleware> = new Set();

    //------------------------------------------------------------------------------------------------------------------

    private async $callBeforeMiddleware(middlewareList : OperationMiddlewareIterable, request : StrataRequest) : Promise<StrataRequest>
    {
        for(const middleware of middlewareList)
        {
            // eslint-disable-next-line no-await-in-loop
            request = (await middleware?.beforeRequest(request)) ?? request;

            // Support ending calls early
            if(request.status !== 'pending')
            {
                break;
            } // end if
        } // end for

        return request;
    } // end if

    private async $callSuccessMiddleware(middlewareList : OperationMiddlewareIterable, request : StrataRequest) : Promise<StrataRequest>
    {
        for(const middleware of middlewareList)
        {
            // eslint-disable-next-line no-await-in-loop
            request = (await middleware?.success(request)) ?? request;
        } // end for

        return request;
    } // end $callSuccessMiddleware

    private async $callFailureMiddleware(middlewareList : OperationMiddlewareIterable, request : StrataRequest) : Promise<StrataRequest>
    {
        for(const middleware of middlewareList)
        {
            // eslint-disable-next-line no-await-in-loop
            request = (await middleware?.failure(request)) ?? request;
        } // end for

        return request;
    } // end $callFailureMiddleware

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Calls an operation, and any middleware configured for the operation.
     *
     * The logic for this function is as follows:
     *
     * 1. Iterated over every configured piece of middleware, and call `middleware.beforeRequest`.
     *   * If the middleware  throws an error, or succeeds/fails the request, we break out of the loop and return the request.
     * 2. Call the operation function
     *   * If the operation function completes, we check to see if it's succeeded or failed the request.
     *     * If it succeeds the request, we call `middleware.success` for all configured middleware.
     *     * If it fails the request, we call `middleware.failure` for all configured middleware.
     *     * If it hasn't done either, we succeed the request with the return value of the operation function, and then call `middleware.success` for all configured middleware.
     *   * If the operation throws, we fail the request and then call `middleware.failure` for all configured middleware.
     *
     * _Note: Middleware is always called in the order of context configured middleware, and then operation configured middleware._
     *
     * @param opName - The name of the operation to call.
     * @param request - The service request request received.
     *
     * @returns Returns a succeeded or failed request. (In the event of an error, it may throw, in which case the
     * caller is expected to handle succeeding or failing the request.)
     */
    public async $callOperation(opName : string, request : StrataRequest) : Promise<StrataRequest>
    {
        const opConfig = this.#operations.get(opName);
        if(!opConfig)
        {
            throw new UnknownOperationError(opName);
        } // end if

        // Call the context level before middleware
        request = await this.$callBeforeMiddleware(this.#middleware, request);

        // Call the operation configured middleware
        request = await this.$callBeforeMiddleware(opConfig.middleware, request);

        // Middleware is allowed to succeed or fail the request early, without calling the operation function.
        if(request.status !== 'pending')
        {
            return request;
        }
        else
        {
            // Call operation
            return opConfig.opFunc(request)
                .then(async(response : ValidPayloadType) =>
                {
                    if(request.status === 'pending')
                    {
                        request.succeed(response);
                    } // end if

                    if(request.status === 'succeeded')
                    {
                        request = await this.$callSuccessMiddleware(this.#middleware, request);
                        request = await this.$callSuccessMiddleware(opConfig.middleware, request);
                    }
                    else if(request.status === 'failed')
                    {
                        request = await this.$callFailureMiddleware(this.#middleware, request);
                        request = await this.$callFailureMiddleware(opConfig.middleware, request);
                    } // end if

                    return request;
                })
                .catch(async(error) =>
                {
                    request.fail(error);
                    request = await this.$callFailureMiddleware(this.#middleware, request);
                    request = await this.$callFailureMiddleware(opConfig.middleware, request);

                    return request;
                });
        } // end if
    } // end $callOperation

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Declares a function to be an exposed operation. The function will be called with a [[StrataRequest]] representing
     * the pending request, and is expected to return a response, or to call [[StrataRequest.success]] or
     * [[StrataRequest.fail]] itself.
     *
     * @param opName - The name we want the function to be exposed under.
     * @param opFunc - The actual function to expose.
     * @param middleware - An optional list of middleware to configure for this function.
     */
    public registerOperation<PayloadType extends ValidPayloadType = ValidPayloadType, Metadata extends ValidMetadataType = ValidMetadataType >(
        opName : string,
        opFunc : OperationHandler<PayloadType, Metadata>,
        middleware : OperationMiddleware[] = []
    ) : void
    {
        this.#operations.set(opName, { opFunc, middleware });
    } // end registerOperation

    /**
     * Exposes all methods of an instance, that meet the specified criteria, as operations (all methods that begin
     * with underscore (_) or dollar sign ($) will automatically be excluded). Operations that are exposed with this method
     * will be simple pass-through methods. The operation names will match the method names of the instance.
     * Middleware should be used to handle any required functionality before and/or after the method call.
     *
     * @param instance - An instance that will have the methods exposed as operations.
     * @param middleware - An optional list of middleware to configure for all methods exposed as operations.
     * @param opModifiers - An optional object to include/exclude specific methods of the instance. Items in the
     * exclude list will take precedence over the include list.
     */
    public registerOperationsForInstance<T>(
        instance : T,
        middleware : OperationMiddleware[] = [],
        opModifiers : { include ?: string[], exclude ?: string[] } = { include: [], exclude: [] }
    ) : void
    {
        const instancePrototype = Object.getPrototypeOf(instance);
        // loop over all properties of the class instance
        Object.getOwnPropertyNames(instancePrototype)
            .forEach((name) =>
            {
                const descriptor = Object.getOwnPropertyDescriptor(instancePrototype, name);
                if(!descriptor) { return; }

                // only handle the instance property if it is a function, is not the constructor, and is not 'private'.
                if(typeof descriptor.value != 'function'
                    || name == 'constructor'
                    || name.startsWith('_')
                    || name.startsWith('$')
                )
                {
                    return;
                }

                // map the method to an operation if it meets the specified criteria.
                if(((opModifiers.include || []).length === 0 || opModifiers.include?.includes(name))
                    && ((opModifiers.exclude || []).length === 0 || !opModifiers.exclude?.includes(name)))
                {
                    this.registerOperation(name, async(request) =>
                    {
                        return instance[name](request.payload);
                    }, middleware);
                }
            });
    } // end registerOperationsForInstance

    /**
     * Registers a piece of middleware to be used for all calls on this context.
     *
     * @param middleware - The middleware to register.
     */
    public useMiddleware(middleware : OperationMiddleware) : void
    {
        this.#middleware.add(middleware);
    } // end useMiddleware

    /**
     * Gets a list of valid operation names.
     *
     * @returns Returns a list of all the valid operations, by name.
     */
    public getOperationNames() : string[]
    {
        return Array.from(this.#operations.keys());
    } // end getOperationNames
} // end StrataContext

//----------------------------------------------------------------------------------------------------------------------
