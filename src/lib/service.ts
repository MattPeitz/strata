//----------------------------------------------------------------------------------------------------------------------
// StrataService
//----------------------------------------------------------------------------------------------------------------------

import { hostname } from 'os';
import { v4 } from 'uuid';
import { version } from '../../package.json';
import envConfig from '@strata-js/util-env-config';
import { logging } from '@strata-js/util-logging';

// Interfaces
import { InternalRequestEnvelope } from '../interfaces/request';
import { ResponseEnvelope } from '../interfaces/response';
import { LocalStorage } from '../interfaces/localStorage';
import { OperationMiddleware } from '../interfaces/middleware';
import { ValidPayloadType } from '../interfaces/payload';
import { DiscoveredServiceGroups } from '../interfaces/discovery';

// Managers
import contextMan from '../managers/context';

// Internal Classes
import { StrataContext } from './context';
import { StrataRequest } from './request';
import { StrataClient } from './client';

// Utils
import localStorageUtil from '../utils/localStorage';
import { MessageQueueUtil } from '../utils/mq';
import { parseOptions } from '../utils/args';

// Libs
import { AlreadyInitializedError, UninitializedError } from './errors';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('service');

//----------------------------------------------------------------------------------------------------------------------

export class StrataService
{
    readonly #id : string = v4();
    readonly #version : string = version;
    readonly #middleware : Set<OperationMiddleware> = new Set();
    #name = 'UnknownService';
    #queue = 'Requests:UnknownService';
    #concurrency = 16;

    // Internal properties
    #mq ?: MessageQueueUtil;
    #initialized = false;
    readonly #client : StrataClient;
    #outstandingRequests : Map<string, StrataRequest> = new Map();

    //------------------------------------------------------------------------------------------------------------------

    constructor()
    {
        const opts = parseOptions();
        this.#id = opts.id as string ?? hostname() ?? v4();

        // Create a client instance
        this.#client = new StrataClient();
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get id() : string { return this.#id; }
    get name() : string { return this.#name; }
    get queue() : string { return this.#queue; }
    get version() : string { return this.#version; }
    get concurrency() : number { return this.#concurrency; }
    get initialized() : boolean { return this.#initialized; }
    get outstandingRequests() : Map<string, StrataRequest> { return this.#outstandingRequests; }

    //------------------------------------------------------------------------------------------------------------------
    // Internal Methods
    //------------------------------------------------------------------------------------------------------------------

    private async $callBeforeMiddleware(request : StrataRequest) : Promise<StrataRequest>
    {
        for(const middleware of this.#middleware)
        {
            // eslint-disable-next-line no-await-in-loop
            request = (await middleware?.beforeRequest(request)) ?? request;

            // Support ending calls early
            if(request.status !== 'pending')
            {
                break;
            } // end if
        } // end for

        return request;
    } // end if

    private async $callSuccessMiddleware(request : StrataRequest) : Promise<StrataRequest>
    {
        for(const middleware of this.#middleware)
        {
            // eslint-disable-next-line no-await-in-loop
            request = (await middleware?.success(request)) ?? request;
        } // end for

        return request;
    } // end $callSuccessMiddleware

    private async $callFailureMiddleware(request : StrataRequest) : Promise<StrataRequest>
    {
        for(const middleware of this.#middleware)
        {
            // eslint-disable-next-line no-await-in-loop
            request = (await middleware?.failure(request)) ?? request;
        } // end for

        return request;
    } // end $callFailureMiddleware

    private async $highWaterMarkCheck() : Promise<boolean>
    {
        // TODO: We might have other checks in the future
        return this.#outstandingRequests.size < (this.#concurrency);
    }

    private $setServiceOptions() : void
    {
        this.#concurrency = envConfig.get<number>('service.concurrency', 16);
    } // end $setServiceOptions

    //------------------------------------------------------------------------------------------------------------------

    public async $handleRequest(requestEnvelope : InternalRequestEnvelope) : Promise<void>
    {
        if(!this.#initialized)
        {
            throw new UninitializedError('call $handleRequest', 'service');
        } // end if

        let request = new StrataRequest(requestEnvelope);
        const store : LocalStorage = {
            currentRequestID: request.id,
            currentRequest: request,
            messages: []
        };

        // Track this outstanding request.
        this.#outstandingRequests.set(request.id, request);

        // Run inside the local storage context.
        await localStorageUtil.run(store, async() =>
        {
            try
            {
                // Get the context, and if it's not found, this will throw with an `UnknownContextError`.
                const context = contextMan.getContext(request.context);

                // Handle global before middleware
                request = await this.$callBeforeMiddleware(request);

                // Call the context handler
                request = await context.$callOperation(request.operation, request);
            }
            catch (error)
            {
                request.fail(error);
            } // end try/catch

            if(request.status === 'succeeded')
            {
                // Handle global middleware success
                request = await this.$callSuccessMiddleware(request);
            }
            else if(request.status === 'failed')
            {
                // Handle global middleware failure
                request = await this.$callFailureMiddleware(request);
            } // end if
        })
            .catch((error) =>
            {
                request.fail(error);
                logger.error('Unhandled exception running request handler. Error:', error.stack);
            });

        // Response to the request
        await this.#mq?.respond(request);

        // Remove this request from our tracking
        this.outstandingRequests.delete(request.id);
    } // end $handleRequest

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Initialize the service, and start listening for incoming messages. This function should be the last thing you
     * call, after registering all middleware and contexts. It takes a service name (to use for service discovery), and
     * a configuration.
     *
     * In the event you simply want to load a local configuration, the recommendation is to do so as follows:
     *
     * ```typescript
     * import { service } from 'strata'
     * import config from './config';
     *
     * // ...Register your middleware and contexts...
     *
     * service.init('MyService', config);
     * ```
     *
     * You can, of course, retrieve your configuration any way you would like before hand, and pass it in.
     *
     * @param serviceName - The name of the service to use for service discovery.
     * @param config - The configuration for this service.
     */
    public async init(serviceName : string, config ?: Record<string, unknown>) : Promise<void>
    {
        if(!this.#initialized)
        {
            this.#name = serviceName;

            // If this was passed in, we parse it.
            if(config)
            {
                envConfig.parseConfig(config);
                logging.setConfig(envConfig.get('logging', { level: 'debug', prettyPrint: true }));
            } // end config

            // Set service options from config
            this.$setServiceOptions();

            // Set the queue name
            this.#queue = `Requests:${ serviceName }`;

            // Initialize our client for service to service calls
            await this.#client.init(serviceName, envConfig.get('redis'));

            // Set up our message queue
            this.#mq = new MessageQueueUtil(envConfig.get('redis'));

            // Initialize the message queue
            await this.#mq.init(this.#name, this.#queue, this.#id);

            // Now we're initialized
            this.#initialized = true;
        }
        else
        {
            throw new AlreadyInitializedError('service');
        } // end if

        // Listen for requests
        this.#mq.listen<InternalRequestEnvelope>(this.#queue, this.#name, this.#id, this.$handleRequest.bind(this), this.$highWaterMarkCheck.bind(this));

        // Start up message
        logger.info(`${ this.#name } v${ this.#version } listening on queue '${ this.#queue }' under service group '${ this.#name }'.`);
    } // end init

    /**
     * Registers a context with the service. All registered operation functions will be exposed via this context.
     *
     * @param contextName - The name to expose this context under.
     * @param context - The context to register.
     */
    public registerContext(contextName : string, context : StrataContext) : void
    {
        contextMan.register(contextName, context);
    } // end registerContext

    /**
     * Registers a piece of middleware to be used for all operation calls. Middleware registered with this method are
     * considered 'global' middleware.
     *
     * @param middleware - The middleware to register.
     */
    public useMiddleware(middleware : OperationMiddleware) : void
    {
        this.#middleware.add(middleware);
    } // end useMiddleware

    /**
     * Directly load configuration. This allows you to set the configuration directly, if you're loading it from an
     * external source.
     *
     * @param config - An object with keys and values.
     */
    public setConfig(config : Record<string, unknown>) : void
    {
        envConfig.setConfig(config);
        logging.setConfig(envConfig.get('logging', { level: 'debug', prettyPrint: true }));

        // Set service options from config
        this.$setServiceOptions();
    } // end setConfig

    /**
     * Parse the config, substituting environment variables, and cascading based on environment.
     *
     * @param config - An object with keys and values.
     */
    public parseConfig(config : Record<string, unknown>) : void
    {
        envConfig.parseConfig(config);
        logging.setConfig(envConfig.get('logging', { level: 'debug', prettyPrint: true }));

        // Set service options from config
        this.$setServiceOptions();
    } // end parseConfig

    /**
     * Sets the number of messages this service will process at any given time.
     *
     * @param concurrency - the concurrency of the service.
     */
    public setConcurrency(concurrency : number) : void
    {
        this.#concurrency = concurrency;
    } // end setConcurrency

    /**
     * Sends a service to service request. This request is tracked, so not only will it know that it was made on behalf
     * of another service call, but that call will have a list of calls that it spawned.
     *
     * If, as part of handling a request, you need to call another service, this is the way to do so.
     *
     * @param queueOrService - Attempts to look up this string as a service name in the config, otherwise passes this
     * as the queue name.
     * @param context - The context for your new request.
     * @param operation - The operation for your new request.
     * @param payload - The payload of your new request.
     * @param metadata - Any metadata for the request. (This is useful for tracking and statistics.)
     * @param auth - The security token for your request, if required.
     * @param timeout - A desired timeout for the request. (Defaults to no timeout.)
     *
     * @returns Returns a completed request instance.
     */
    public async request<PayloadType extends ValidPayloadType = Record<string, unknown>>(
        queueOrService : string,
        context : string,
        operation : string,
        payload : Record<string, unknown>,
        metadata ?: Record<string, unknown>,
        auth ?: string,
        timeout ?: number
    ) : Promise<ResponseEnvelope<PayloadType>>
    {
        const queue = envConfig.get(`services.${ queueOrService }`, queueOrService);
        return this.#client.request<PayloadType>(queue, context, operation, payload, metadata, auth, timeout);
    } // end request

    /**
     * Returns an object listing all discovered service groups, the queues those groups were found on, the contexts
     * supported by services on those groups, and list of consumers on those groups. The intent of this is to allow for
     * clients that do not know the service topology to be able to discover services, and make a reasonable best attempt
     * at calling them.
     *
     * This endpoint should not be called too often, as it could have a performance impact under high loads.
     *
     * @returns Returns all discovered service groups.
     */
    public async discoverServiceGroups() : Promise<DiscoveredServiceGroups>
    {
        return this.#client.discoverServiceGroups();
    }

    /**
     * Shuts down and cleans up the service. **This should never be called from external code.** (It is only public,
     * because it needs to be exposed beyond this class.)
     */
    async teardown() : Promise<void>
    {
        if(this.#mq)
        {
            await this.#mq.stopListeningForResponse(this.queue, this.name, this.id);
        } // end if

        // Wait for all of the outstanding requests to be finished.
        await Promise.all([
            ...this.#outstandingRequests.values()
        ]
            .map((request) => request.promise.catch()));

        // Cleanup #mq.
        if(this.#mq)
        {
            await this.#mq.teardown();
        } // end if
    }
} // end StrataService

//----------------------------------------------------------------------------------------------------------------------
