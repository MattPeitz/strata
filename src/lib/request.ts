//----------------------------------------------------------------------------------------------------------------------
// StrataRequest
//----------------------------------------------------------------------------------------------------------------------

import { v4 } from 'uuid';

// Interfaces
import { ResponseEnvelope, ResponseMessage } from '../interfaces/response';
import { InternalRequestEnvelope, RequestEnvelope } from '../interfaces/request';
import { LocalStorage } from '../interfaces/localStorage';
import { ValidPayloadType, ValidMetadataType } from '../interfaces/payload';

// Utils
import localStorageUtil from '../utils/localStorage';
import { Optional } from '../utils/types';
import { ServiceError } from './errors';

//----------------------------------------------------------------------------------------------------------------------

type RequestConstructor = Optional<InternalRequestEnvelope<ValidPayloadType, ValidMetadataType>, 'id' | 'timestamp' | 'internalID'>

//----------------------------------------------------------------------------------------------------------------------

export class StrataRequest<PayloadType extends ValidPayloadType = ValidPayloadType, MetadataType extends ValidMetadataType = ValidMetadataType>
{
    // Shared Properties
    readonly id : string;
    readonly context : string;
    readonly operation : string;

    // Request Properties
    readonly internalID ?: string;
    readonly responseQueue ?: string;
    readonly receivedTimestamp = Date.now();
    readonly timestamp : string;
    readonly timeout ?: number;
    readonly payload : PayloadType;
    readonly metadata : MetadataType;
    readonly auth ?: string;
    readonly client : string;
    readonly priorRequest ?: string;

    // Request Properties
    response ?: ValidPayloadType;
    completedTimestamp ?: number;
    status : 'succeeded' | 'failed' | 'pending' = 'pending';
    requestChain : string[] = [];
    service ?: string;

    // Internal Properties
    readonly promise : Promise<void>;
    #resolve : (value ?: void | PromiseLike<void>) => void = () => undefined;

    //------------------------------------------------------------------------------------------------------------------

    constructor(request : RequestConstructor, serviceString ?: string)
    {
        this.id = request.id ?? v4();
        this.internalID = request.internalID;
        this.context = request.context;
        this.operation = request.operation;
        this.responseQueue = request.responseQueue;
        this.timestamp = request.timestamp ?? (new Date()).toISOString();
        this.timeout = request.timeout;
        this.payload = request.payload as PayloadType;
        this.metadata = request.metadata as MetadataType;
        this.auth = request.auth;
        this.client = request.client ?? 'unknown';
        this.priorRequest = request.priorRequest;

        // Specifying this allows us to name the service, without depending on the service code.
        this.service = serviceString;

        // Build a new promise to track when this is succeeded or failed.
        this.promise = new Promise((resolve) =>
        {
            this.#resolve = resolve;
        });
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------

    get localStore() : LocalStorage | undefined
    {
        return localStorageUtil.getStore();
    }

    get messages() : ResponseMessage[] | undefined { return this?.localStore?.messages; }

    //------------------------------------------------------------------------------------------------------------------

    public parseResponse(response : ResponseEnvelope) : void
    {
        this.completedTimestamp = (new Date(response.timestamp)).getTime();
        this.requestChain = response.requestChain ?? [];
        this.service = response.service;

        // Populate messages array
        response.messages.forEach((msg) => this.messages?.push(msg));

        if(response.status === 'succeeded')
        {
            this.succeed(response.payload);
        }
        else
        {
            this.fail(response.payload);
        } // end if
    } // end response

    /**
     * Renders the on-the-wire format for the request envelope.
     *
     * @returns Returns a plain object version of the request envelope.
     */
    public renderRequest() : RequestEnvelope<PayloadType, MetadataType>
    {
        return {
            id: this.id,
            context: this.context,
            operation: this.operation,
            responseQueue: this.responseQueue,
            timestamp: (new Date(this.receivedTimestamp)).toISOString(),
            payload: this.payload as PayloadType,
            metadata: this.metadata as MetadataType,
            auth: this.auth,
            priorRequest: this.priorRequest,
            client: this.client
        };
    } // end renderRequest

    /**
     * Renders the on-the-wire format for the response envelope.
     *
     * @returns Returns a plain object version of the response envelope.
     */
    public renderResponse<ResponsePayloadType extends ValidPayloadType = Record<string, unknown>>() : ResponseEnvelope<ResponsePayloadType>
    {
        return {
            id: this.id,
            context: this.context,
            operation: this.operation,
            timestamp: (new Date(this.completedTimestamp ?? Date.now())).toISOString(),
            payload: this.response as ResponsePayloadType ?? {},
            status: this.status,
            messages: this.messages ?? [],
            requestChain: this.requestChain,
            service: this.service ?? 'unknown'
        };
    } // end renderResponse

    /**
     * Succeeds this request.
     *
     * @param payload
     */
    public succeed(payload : ValidPayloadType) : void
    {
        this.response = payload;
        this.status = 'succeeded';
        this.completedTimestamp = this.completedTimestamp ?? Date.now();

        // Resolve promise
        this.#resolve();
    } // end succeed

    public fail(reason : string | Error | ValidPayloadType) : void
    {
        if(reason instanceof ServiceError)
        {
            this.response = reason.toJSON();
        }
        else if(reason instanceof Error)
        {
            this.response = {
                message: reason.message,
                name: 'FailedRequestError',
                stack: reason.stack,
                code: reason['code'] ?? 'FAILED_REQUEST',
                details: reason['details']
            };
        }
        else if(typeof reason === 'string')
        {
            this.response = {
                message: reason,
                name: 'FailedRequestError',
                code: 'FAILED_REQUEST'
            };
        }
        else
        {
            this.response = reason;
        } // end if

        this.status = 'failed';
        this.completedTimestamp = this.completedTimestamp ?? Date.now();

        // Resolve promise
        this.#resolve();
    } // end fail
} // end StrataRequest

//----------------------------------------------------------------------------------------------------------------------
