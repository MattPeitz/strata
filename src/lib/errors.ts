// ---------------------------------------------------------------------------------------------------------------------
// Custom Errors
// ---------------------------------------------------------------------------------------------------------------------

import { StrataRequest } from './request';
import { ResponseEnvelope } from '../interfaces/response';
import { RequestEnvelope } from '../interfaces/request';

// ---------------------------------------------------------------------------------------------------------------------

export class ServiceError extends Error
{
    public code = 'SERVICE_ERROR';

    constructor(message : string)
    {
        super(message);
    } // end constructor

    public toJSON() : { name : string, message : string, code : string, stack ?: string }
    {
        return {
            name: this.constructor.name,
            message: this.message,
            code: this.code,
            stack: this.stack
        };
    } // end toJSON
} // end ServiceError

// ---------------------------------------------------------------------------------------------------------------------

export class UnknownContextError extends ServiceError
{
    public code = 'UNKNOWN_CONTEXT_ERROR';

    constructor(context ?: string)
    {
        super(`Missing or unrecognized context${ context ? ` '${ context }'` : '' }.`);
    } // end constructor
} // end UnknownContextError

// ---------------------------------------------------------------------------------------------------------------------

export class UnknownOperationError extends ServiceError
{
    public code = 'UNKNOWN_OPERATION_ERROR';

    constructor(operation ?: string)
    {
        super(`Missing or unrecognized operation${ operation ? ` '${ operation }'` : '' }.`);
    } // end constructor
} // end UnknownOperationError

// ---------------------------------------------------------------------------------------------------------------------

export class FailedRequestError extends ServiceError
{
    public code = 'FAILED_REQUEST';
    public details ?: Record<string, unknown>;
    #response : ResponseEnvelope<Record<string, unknown>>;

    constructor(request : StrataRequest)
    {
        super((request.response && (request.response['message'] as string)) ?? 'Request failed.');

        this.#response = request.renderResponse();
        if(request.response && request.response.details)
        {
            // Populate details, if it exists.
            this.details = request.response.details as Record<string, unknown>;
        }
    } // end constructor

    public toJSON() : { name : string, message : string, code : string, stack ?: string, [ key : string ] : unknown }
    {
        return {
            name: this.constructor.name,
            message: this.message,
            code: this.code,
            stack: this.stack,
            response: this.#response
        };
    } // end toJSON
} // end FailedRequestError

// ---------------------------------------------------------------------------------------------------------------------

export class InternalTimeoutError extends ServiceError
{
    public code = 'INTERNAL_TIMEOUT_ERROR';

    constructor(maxTime : number)
    {
        super(`Operation timed out after ${ maxTime }ms.`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class RequestTimeoutError extends ServiceError
{
    public code = 'REQUEST_TIMEOUT_ERROR';
    readonly request : StrataRequest;

    constructor(request : StrataRequest)
    {
        super(`Request '${ request.id }' timed out.`);
        this.request = request;
    }

    public toJSON() : { name : string, message : string, code : string, stack ?: string, request : RequestEnvelope }
    {
        return {
            name: this.constructor.name,
            message: this.message,
            code: this.code,
            stack: this.stack,
            request: this.request.renderRequest()
        };
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class AlreadyInitializedError extends ServiceError
{
    public code = 'ALREADY_INITIALIZED_ERROR';

    constructor(type : 'service' | 'client' = 'service')
    {
        super(`Initialization failed: ${ type } already initialized!`);
    } // end constructor
} // end UnknownOperationError

// ---------------------------------------------------------------------------------------------------------------------

export class UninitializedError extends ServiceError
{
    public code = 'UNINITIALIZED_ERROR';

    constructor(attemptedAction : string, type : 'service' | 'client' = 'service')
    {
        super(`Attempted to ${ attemptedAction } without initializing the ${ type }!`);
    } // end constructor
} // end UnknownOperationError

// ---------------------------------------------------------------------------------------------------------------------
