//----------------------------------------------------------------------------------------------------------------------
// StrataClient
//----------------------------------------------------------------------------------------------------------------------

import { hostname } from 'os';
import { v4 } from 'uuid';
import { version } from '../../package.json';
import envConfig from '@strata-js/util-env-config';
import { logging } from '@strata-js/util-logging';

// Interfaces
import { ValidPayloadType } from '../interfaces/payload';
import { ResponseEnvelope } from '../interfaces/response';
import { ServiceGroup, DiscoveredServiceGroups } from '../interfaces/discovery';

// Classes
import { StrataRequest } from './request';

// Utils
import { MessageQueueUtil } from '../utils/mq';
import { parseOptions } from '../utils/args';
import localStorageUtil from '../utils/localStorage';
import shutdownUtil from '../utils/shutdown';
import { promiseTimeout } from '../utils/timeout';

// Lib
import { AlreadyInitializedError, FailedRequestError, RequestTimeoutError, UninitializedError } from './errors';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('client');

//----------------------------------------------------------------------------------------------------------------------

export class StrataClient
{
    readonly #id : string = v4();
    readonly #version : string = version;
    #name = 'UnknownClient';

    // Internal properties
    #mq ?: MessageQueueUtil;
    #initialized = false;
    #outstandingRequests : Map<string, StrataRequest> = new Map();

    constructor()
    {
        const opts = parseOptions();
        this.#id = opts.id as string ?? hostname() ?? v4();

        // Register this client with the shutdown util
        shutdownUtil.registerClient(this);
    }

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get id() : string { return this.#id; }
    get name() : string { return this.#name; }
    get version() : string { return this.#version; }
    get initialized() : boolean { return this.#initialized; }
    get responseQueue() : string { return `Responses:${ this.#name }:${ this.#id }`; }
    get outstandingRequests() : Map<string, StrataRequest> { return this.#outstandingRequests; }

    //------------------------------------------------------------------------------------------------------------------
    // Internal Methods
    //------------------------------------------------------------------------------------------------------------------

    public async $handleResponse(responseEnvelope : ResponseEnvelope) : Promise<void>
    {
        if(!this.#initialized)
        {
            throw new UninitializedError('call $handleResponse', 'client');
        } // end if

        const request = this.#outstandingRequests.get(responseEnvelope.id);
        if(request)
        {
            request.parseResponse(responseEnvelope);
        }
        else
        {
            logger.warn(`Got response for unknown request '${ responseEnvelope.id }'.`);
        } // end if
    } // end $handleResponse

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Initialize the client. It takes a client name (to use for tracking purposes), and a configuration.
     *
     * @param clientName - The name of the client to use for client discovery.
     * @param config - The configuration for this client.
     */
    public async init(clientName : string, config : Record<string, unknown>) : Promise<void>
    {
        if(!this.#initialized)
        {
            this.#name = clientName;

            // Set up our message queue
            this.#mq = new MessageQueueUtil(config);

            // Initialize the message queue
            await this.#mq.init(this.#name, this.responseQueue, this.#id);

            this.#initialized = true;
        }
        else
        {
            throw new AlreadyInitializedError('client');
        } // end if

        // Listen for responses
        this.#mq.listen<ResponseEnvelope>(this.responseQueue, this.#name, this.#id, this.$handleResponse.bind(this));
    } // end init

    /**
     * Sends a service request.
     *
     * @param queueOrService - Attempts to look up this string as a service name in the config, otherwise passes this
     * as the queue name.
     * @param context - The context for your new request.
     * @param operation - The operation for your new request.
     * @param payload - The payload of your new request.
     * @param metadata - Any metadata for the request. (This is useful for tracking and statistics.)
     * @param auth - The security token for your request, if required.
     * @param timeout - A desired timeout for the request. (Defaults to no timeout.)
     *
     * @returns Returns a completed request instance.
     */
    public async request<PayloadType extends ValidPayloadType = Record<string, unknown>>(
        queueOrService : string,
        context : string,
        operation : string,
        payload : Record<string, unknown>,
        metadata ?: Record<string, unknown>,
        auth ?: string,
        timeout ?: number
    ) : Promise<ResponseEnvelope<PayloadType>>
    {
        const queue = envConfig.get(`services.${ queueOrService }`, queueOrService);

        if(this.#initialized && this.#mq)
        {
            const store = localStorageUtil.getStore();
            metadata = metadata ?? {};

            const request = new StrataRequest({
                context,
                operation,
                payload,
                metadata,
                auth,
                timeout,
                responseQueue: this.responseQueue,
                client: `${ this.name } v${ this.version }`,
                priorRequest: store ? store.currentRequestID : undefined
            });

            if(store && store.currentRequest)
            {
                store.currentRequest.requestChain.push(request.id);
            }

            // Track this outstanding request.
            this.#outstandingRequests.set(request.id, request);

            // Send the message to redis.
            await this.#mq.request(queue, request);

            // Wait for the promise to be fulfilled.
            await promiseTimeout(request.promise, request.timeout)
                .catch((error) =>
                {
                    if(error?.code === 'INTERNAL_TIMEOUT_ERROR')
                    {
                        throw new RequestTimeoutError(request);
                    }

                    throw error;
                });

            // Remove tracking of the request
            this.#outstandingRequests.delete(request.id);

            if(request.status === 'failed')
            {
                throw new FailedRequestError(request);
            }

            // Return the response
            return request.renderResponse<PayloadType>();
        }
        else
        {
            throw new UninitializedError('make a request', 'client');
        } // end if
    } // end request

    /**
     * Returns an object listing all discovered service groups, the queues those groups were found on, the contexts
     * supported by services on those groups, and list of consumers on those groups. The intent of this is to allow for
     * clients that do not know the service topology to be able to discover services, and make a reasonable best attempt
     * at calling them.
     *
     * This endpoint should not be called too often, as it could have a performance impact under high loads.
     *
     * @params maxConsumerIdleTime - The maximum idle time (in miliseconds) for a consumer with pending requests to be
     * considered 'dead' and skipped over. (Default: 15m)
     *
     * @returns Returns all discovered service groups.
     */
    async discoverServiceGroups(maxConsumerIdleTime : number = 15 * 60 * 1000) : Promise<DiscoveredServiceGroups>
    {
        if(this.#initialized && this.#mq)
        {
            const services : Map<string, ServiceGroup> = new Map();
            const queueList = await this.#mq.discoverQueues();
            await Promise.all(Object.keys(queueList)
                .filter((queueName) =>
                {
                    return queueName.startsWith('Requests:');
                })
                .map(async(queueName) =>
                {
                    const { groups } = queueList[queueName];
                    await Promise.all(groups.map(async(group) =>
                    {
                        let serviceGroup = services.get(group.name);
                        if(!serviceGroup)
                        {
                            serviceGroup = {
                                queues: {}
                            };

                            services.set(group.name, serviceGroup);
                        }

                        let contexts = {};

                        // Get a list of all consumers that have either finished all their work, or haven't been idle for longer than 2 minutes.
                        const consumers = (group.consumers ?? [])
                            .filter((consumer) => consumer.pending === 0 || consumer.idle < maxConsumerIdleTime);

                        if(consumers.length > 0)
                        {
                            // Get a list of contexts
                            const serviceInfoRequest = await this.request(queueName, 'service', 'info', {}, {}, undefined, 10 * 1000)
                                .catch((error) =>
                                {
                                    logger.warn('Service info request failed:', error.message);
                                });

                            if(serviceInfoRequest && serviceInfoRequest.status === 'succeeded')
                            {
                                contexts = serviceInfoRequest.payload.actions as Record<string, string[]>;
                            }
                        }

                        serviceGroup.queues[queueName] = {
                            contexts,
                            consumers: group.consumers ?? [],
                            pending: group.pending
                        };
                    }));
                }));

            return Object.fromEntries(services);
        }
        else
        {
            throw new UninitializedError('discover services', 'client');
        }
    }

    /**
     * Shuts down and cleans up the client. **This should never be called from external code.** (It is only public,
     * because it needs to be exposed beyond this class.)
     */
    async teardown() : Promise<void>
    {
        // Wait for all of the outstanding requests to be finished.
        await Promise.all([
            ...this.#outstandingRequests.values()
        ]
            .map((request) => request.promise.catch()));

        if(this.#mq)
        {
            await this.#mq.stopListeningForResponse(this.responseQueue, this.name, this.id);
            await this.#mq.teardown();
        } // end if
    }
} // end StrataClient

//----------------------------------------------------------------------------------------------------------------------
