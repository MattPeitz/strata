// ---------------------------------------------------------------------------------------------------------------------
// Context Interfaces
// ---------------------------------------------------------------------------------------------------------------------

import { StrataRequest } from '../lib/request';
import { ValidPayloadType } from './payload';

// ---------------------------------------------------------------------------------------------------------------------

export type OperationHandler<
    P extends ValidPayloadType = ValidPayloadType,
    M extends ValidPayloadType = ValidPayloadType
    > = (request : Readonly<StrataRequest<P, M>>) => Promise<ValidPayloadType>;

// ---------------------------------------------------------------------------------------------------------------------
