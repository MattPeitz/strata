// ---------------------------------------------------------------------------------------------------------------------
// Request Interfaces
// ---------------------------------------------------------------------------------------------------------------------

import { ValidPayloadType, ValidMetadataType } from './payload';

//----------------------------------------------------------------------------------------------------------------------

export interface RequestEnvelope<
        PayloadType extends ValidPayloadType = Record<string, unknown>,
        MetadataType extends ValidMetadataType = Record<string, unknown>
    >
{
    id : string;
    context : string;
    operation : string;
    responseQueue ?: string;
    timestamp : string;
    timeout ?: number;
    payload : PayloadType;
    metadata : MetadataType;
    auth ?: string;
    priorRequest ?: string;
    client ?: string;
}

export interface InternalRequestEnvelope<
        PayloadType extends ValidPayloadType = Record<string, unknown>,
        MetadataType extends ValidMetadataType = Record<string, unknown>
    > extends RequestEnvelope<PayloadType, MetadataType>
{
    internalID : string;
}

// ---------------------------------------------------------------------------------------------------------------------
