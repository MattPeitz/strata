// ---------------------------------------------------------------------------------------------------------------------
// Operation LocalStorage
// ---------------------------------------------------------------------------------------------------------------------

import { ResponseMessage } from './response';
import { StrataRequest } from '../lib/request';

// ---------------------------------------------------------------------------------------------------------------------

export interface LocalStorage
{
    currentRequestID : string;
    currentRequest : StrataRequest;
    user ?: Record<string, unknown>; // TODO: We should define this better. Maybe some way for the service to specify what type to use for user information?
    messages : ResponseMessage[];

    // Allow extra properties to be accessed
    [ key : string ] : unknown;
} // end LocalStorage

// ---------------------------------------------------------------------------------------------------------------------
