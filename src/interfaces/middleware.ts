// ---------------------------------------------------------------------------------------------------------------------
// Middleware Interface
// ---------------------------------------------------------------------------------------------------------------------

import { StrataRequest } from '../lib/request';

// ---------------------------------------------------------------------------------------------------------------------

export interface OperationMiddleware
{
    beforeRequest : (request : StrataRequest) => Promise<StrataRequest | undefined>;
    success : (request : StrataRequest) => Promise<StrataRequest | undefined>;
    failure : (request : StrataRequest) => Promise<StrataRequest | undefined>;
} // end OperationMiddleware

// ---------------------------------------------------------------------------------------------------------------------
