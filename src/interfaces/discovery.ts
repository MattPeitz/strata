// ---------------------------------------------------------------------------------------------------------------------
// Interfaces related to Service Discovery
// ---------------------------------------------------------------------------------------------------------------------

export interface ConsumerDetails {
    name : string;
    pending : number;
    idle : number;
}

export interface ServiceGroupDetails {
    name : string;
    consumers ?: ConsumerDetails[];
    pending : number;
    'last-delivered-id' : string;
}

export interface QueueDetails {
    groups : ServiceGroupDetails[];
}

export interface DiscoveredQueues {
    [queueName : string] : QueueDetails;
}

export interface ServiceGroup {
    queues : {
        [queueName : string] : {
            contexts : Record<string, string[]>;
            consumers : ConsumerDetails[];
            pending : number;
        }
    };
}

export interface DiscoveredServiceGroups {
    [serviceGroupName : string] : ServiceGroup
}

// ---------------------------------------------------------------------------------------------------------------------
