// ---------------------------------------------------------------------------------------------------------------------
// Response Interfaces
// ---------------------------------------------------------------------------------------------------------------------

import { ValidPayloadType } from './payload';

//----------------------------------------------------------------------------------------------------------------------

export interface ResponseMessage<DetailsType = Record<string, unknown>>
{
    message : string;
    code : string;
    type : string;
    details ?: DetailsType;
    severity : 'error' | 'warning' | 'info' | 'debug';
    stack ?: string;
}

export interface ResponseEnvelope<PayloadType extends ValidPayloadType = Record<string, unknown>>
{
    id : string;
    context : string;
    operation : string;
    timestamp : string;
    payload : PayloadType;
    status : 'succeeded' | 'failed' | 'pending';
    messages : ResponseMessage[];
    requestChain ?: string[];
    service : string;
}

// ---------------------------------------------------------------------------------------------------------------------
