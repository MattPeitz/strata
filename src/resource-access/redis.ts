//----------------------------------------------------------------------------------------------------------------------
// RedisResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import IORedis, { Redis } from 'ioredis';
import { ConsumerDetails, ServiceGroupDetails } from '../interfaces/discovery';

//----------------------------------------------------------------------------------------------------------------------

class RedisResourceAccess
{
    public async ensureConsumerGroup(conn : Redis, queue : string, serviceName : string) : Promise<void>
    {
        await conn.xgroup('CREATE', queue, serviceName, '$', 'MKSTREAM')
            .catch((err) =>
            {
                // FIXME: We _should_ be able to look for a `code` property on the error object, but that doesn't seem
                // to work. So instead, we look for it in the message text.
                if(!err.message.includes('BUSYGROUP'))
                {
                    throw err;
                }
            });
    }

    public async createConsumer(conn : Redis, queue : string, serviceName : string, serviceID : string) : Promise<void>
    {
        await conn.xgroup('CREATECONSUMER', queue, serviceName, serviceID);
    }

    public async deleteConsumer(conn : Redis, queue : string, serviceName : string, serviceID : string) : Promise<void>
    {
        await conn.xgroup('DELCONSUMER', queue, serviceName, serviceID);
    }

    public getConnection(connectionConfig : Record<string, unknown>) : Redis
    {
        return new IORedis(connectionConfig);
    } // end getConnection

    public async getClientID(conn : Redis) : Promise<string>
    {
        return conn.client('ID');
    }

    public async listQueues(conn : Redis) : Promise<string[]>
    {
        return new Promise((resolve, reject) =>
        {
            const results : Set<string> = new Set();
            const scanStream = conn.scanStream({ type: 'stream' });

            scanStream.on('data', (resultKeys) =>
            {
                resultKeys.forEach((key) => results.add(key));
            });

            scanStream.on('error', (error) => reject(error));
            scanStream.on('end', () => resolve([ ...results ]));
        });
    }

    public async listGroups(conn : Redis, queue : string) : Promise<ServiceGroupDetails[]>
    {
        const results = await conn.xinfo('GROUPS', queue);
        return results.map((group : string[]) =>
        {
            const groupObj = {};
            group.forEach((keyOrValue, index) =>
            {
                if(index % 2 === 0)
                {
                    groupObj[keyOrValue] = group[index + 1];
                }
            });

            return groupObj;
        });
    }

    public async listConsumers(conn : Redis, queue : string, serviceName : string) : Promise<ConsumerDetails[]>
    {
        const results = await conn.xinfo('CONSUMERS', queue, serviceName);
        return results.map((consumer : string[]) =>
        {
            const consumerObj = {};
            consumer.forEach((keyOrValue, index) =>
            {
                if(index % 2 === 0)
                {
                    consumerObj[keyOrValue] = consumer[index + 1];
                }
            });

            return consumerObj;
        });
    }

    public async blockingRead(conn : Redis, queue : string, serviceName : string, serviceID : string) : Promise<{ id : string, msg : Record<string, unknown> } | undefined>
    {
        const results = await conn.xreadgroup('GROUP', serviceName, serviceID, 'COUNT', 1, 'BLOCK', 0, 'STREAMS', queue, '>');

        if(results)
        {
            // This assumes multiple streams, but we don't support that, so pull out the actual message.
            const result = results[0][1][0];

            // The format of result is: `[ MessageID, [ Key, Value ] ]`, and what we want out is:
            // `{ id: MessageID, msg: Value }`
            return {
                id: result[0],
                msg: JSON.parse(result[1][1])
            };
        }
    } // end blockingRead

    public async insertMessage(conn : Redis, queue : string, message : Record<string, unknown>) : Promise<string>
    {
        const messageJSON = JSON.stringify(message);
        return conn.xadd(queue, '*', 'msg', messageJSON);
    } // end insertMessage

    public async ackMessage(conn : Redis, queue : string, name : string, messageID : string) : Promise<void>
    {
        await conn.xack(queue, name, messageID);
    } // end ackMessage

    public async unblock(conn : Redis, name : string) : Promise<void>
    {
        await conn.client('UNBLOCK', name);
    } // end unblock
} // end RedisResourceAccess

//----------------------------------------------------------------------------------------------------------------------

export default new RedisResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
