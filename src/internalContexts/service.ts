// ---------------------------------------------------------------------------------------------------------------------
// Internal Service Context
// ---------------------------------------------------------------------------------------------------------------------

import { hostname } from 'os';
import envConfig from '@strata-js/util-env-config';

// Managers
import serviceMan from '../managers/service';
import contextMan from '../managers/context';

// Internal Classes
import { StrataContext } from '../lib/context';

// ---------------------------------------------------------------------------------------------------------------------

const context = new StrataContext();

// ---------------------------------------------------------------------------------------------------------------------

context.registerOperation('info', async() =>
{
    const actions = contextMan
        .getContextNames()
        .reduce((accum, contextName) =>
        {
            const contextInst = contextMan.getContext(contextName);
            accum[contextName] = contextInst.getOperationNames();
            return accum;
        }, {});

    return {
        id: serviceMan.service.id,
        name: serviceMan.service.name,
        version: serviceMan.service.version,
        environment: envConfig.env,

        // Note: this assumes we're either setting a `HOSTNAME` variable, or otherwise setting up docker to return the
        // correct hostname. For more information, please see this:
        // https://stackoverflow.com/questions/29808920/how-to-get-the-hostname-of-the-docker-host-from-inside-a-docker-container-on-tha
        hostname: process.env.HOSTNAME ?? hostname(),
        actions
    };
});

// ---------------------------------------------------------------------------------------------------------------------

export default context;

// ---------------------------------------------------------------------------------------------------------------------
