// ---------------------------------------------------------------------------------------------------------------------
// Context Unit Tests
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Lib
import { StrataContext } from '../src/lib/context';

// ---------------------------------------------------------------------------------------------------------------------

describe('Context', () =>
{
    describe('$callOperation()', () =>
    {
        xit('executes the specified operation name', () =>
        {
            // TODO: Implement
        });

        xit('it calls the `beforeRequest` function on all registered middleware', () =>
        {
            // TODO: Implement
        });

        xit('it stop calling `beforeRequest` functions if any middleware throw a error', () =>
        {
            // TODO: Implement
        });

        xit('it stop calling `beforeRequest` functions if any middleware succeed or fail a request', () =>
        {
            // TODO: Implement
        });

        xit('succeeds the request if the operation did not explicitly succeed the request.', () =>
        {
            // TODO: Implement
        });

        xit('fails the request if the operation throws.', () =>
        {
            // TODO: Implement
        });

        xit('calls the `success` function on all registered middleware if the operation succeeds the request', () =>
        {
            // TODO: Implement
        });

        xit('calls the `failure` function on all registered middleware if the operation fails the request', () =>
        {
            // TODO: Implement
        });

        xit('calls context middleware before operation middleware', () =>
        {
            // TODO: Implement
        });

        xit('calls middleware in the order they were registered', () =>
        {
            // TODO: Implement
        });
    });

    describe('registerOperation()', () =>
    {
        xit('allows registering an operation', () =>
        {
            // TODO: Implement
        });

        xit('does not allow overwriting a previously registered operation', () =>
        {
            // TODO: Implement
        });

        xit('supports registering the same operation under multiple names', () =>
        {
            // TODO: Implement
        });

        xit('allows registering middleware for just this operation', () =>
        {
            // TODO: Implement
        });
    });

    describe('registerOperationsForInstance()', () =>
    {
        xit('registers all functions on an object as operations', () =>
        {
            // TODO: Implement
        });

        xit('supports a whitelist by specifying function names in the `includes` list', () =>
        {
            // TODO: Implement
        });

        xit('supports a blacklist by specifying function names in the `excludes` list', () =>
        {
            // TODO: Implement
        });

        xit('throws an error if both `includes` and `excludes` are specified', () =>
        {
            // TODO: Implement
        });
    });

    describe('useMiddleware()', () =>
    {
        xit('adds the specified middleware to the list of context middleware', () =>
        {
            // TODO: Implement
        });
    });

    describe('getOperationNames()', () =>
    {
        it('returns a list of the operation names registered.', () =>
        {
            const context = new StrataContext();
            context.registerOperation('foo', () => Promise.resolve({}));
            context.registerOperation('bar', () => Promise.resolve({}));

            const operationsNames = context.getOperationNames();

            expect(operationsNames).to.be.an.instanceOf(Array);
            expect(operationsNames.length).to.equal(2);
            expect(operationsNames).to.deep.equal([ 'foo', 'bar' ]);
        });

        it('returns an empty array if no operations are registered.', () =>
        {
            const context = new StrataContext();
            const operationsNames = context.getOperationNames();

            expect(operationsNames).to.be.an.instanceOf(Array);
            expect(operationsNames.length).to.equal(0);
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
