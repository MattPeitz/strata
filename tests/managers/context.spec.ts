// ---------------------------------------------------------------------------------------------------------------------
// Context Unit Tests
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Managers
import contextMan from '../../src/managers/context';

// Lib
import { StrataContext } from '../../src/lib/context';
import { UnknownContextError } from '../../src/lib/errors';

// ---------------------------------------------------------------------------------------------------------------------

describe('Context Manager', () =>
{
    beforeEach(() =>
    {
        contextMan.clearContexts();
    });

    describe('register()', () =>
    {
        it('allows registering a context', () =>
        {
            const context = new StrataContext();
            contextMan.register('test', context);

            expect(contextMan.getContextNames()).to.include('test');
        });

        xit('does not allow overwriting a previously registered context', () =>
        {
            // TODO: Implement
        });

        it('supports registering the same context under multiple names', () =>
        {
            const context = new StrataContext();
            contextMan.register('test', context);
            contextMan.register('test2', context);

            expect(contextMan.getContext('test')).to.equal(context);
            expect(contextMan.getContext('test2')).to.equal(context);
        });
    });

    describe('getContext', () =>
    {
        it('returns a context by name', () =>
        {
            const context = new StrataContext();
            contextMan.register('test', context);

            expect(contextMan.getContext('test')).to.equal(context);
        });

        it('throws an error if there is no context with the specified name', () =>
        {
            const getContext = () : void =>
            {
                contextMan.getContext('doesNotExist');
            };

            expect(getContext).to.throw(UnknownContextError);
        });
    });

    describe('getContextNames', () =>
    {
        it('returns a list of the context names registered.', () =>
        {
            const context = new StrataContext();
            contextMan.register('test', context);
            contextMan.register('test2', context);

            const contextNames = contextMan.getContextNames();

            expect(contextNames).to.be.an.instanceOf(Array);
            expect(contextNames.length).to.equal(2);
            expect(contextNames).to.deep.equal([ 'test', 'test2' ]);
        });

        it('returns an empty array if no contexts are registered.', () =>
        {
            const contextNames = contextMan.getContextNames();

            expect(contextNames).to.be.an.instanceOf(Array);
            expect(contextNames.length).to.equal(0);
        });
    });

    describe('clearContexts()', () =>
    {
        it('clears all registered contexts', () =>
        {
            const context = new StrataContext();
            contextMan.register('test', context);

            expect(contextMan.getContextNames()).to.include('test');

            contextMan.clearContexts();
            expect(contextMan.getContextNames().length).to.equal(0);
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
